from kedro.pipeline import node, Pipeline
from .nodes import encode_classes, split_train_test

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=encode_classes,
                inputs="iris_data",
                outputs="preprocessed_iris_data",
                name='class_encoding',
            ),

            node(
                func=split_train_test,
                inputs="preprocessed_iris_data",
                outputs=["x_train","x_test","y_train","y_test"],
                name="train_test_split_node"
            )
        ]
    )