import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split

def encode_classes(iris_df: pd.DataFrame) -> pd.DataFrame:
    le = LabelEncoder()
    le.fit(iris_df['Species'])
    iris_df['Species'] = le.transform(iris_df['Species'])
    return iris_df
    

def split_train_test(iris_df: pd.DataFrame) -> pd.DataFrame:
    X = iris_df.drop(columns=['Species'])
    Y = iris_df['Species']
    x_train, x_test, y_train, y_test = train_test_split(X,Y, test_size=0.2, stratify=Y)
    return x_train,x_test,y_train,y_test