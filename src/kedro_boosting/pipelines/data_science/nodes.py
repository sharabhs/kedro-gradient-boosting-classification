from catboost import CatBoostClassifier
from sklearn.metrics import accuracy_score, classification_report
import numpy as np
import logging
import json


def model_training(x_train: np.ndarray, y_train: np.ndarray) -> CatBoostClassifier:
    model = CatBoostClassifier()
    model.fit(x_train,y_train)
    classifier_model = model
    return classifier_model


def evaluate_model(classifier_model: CatBoostClassifier, x_test: np.ndarray, y_test: np.ndarray) -> None:
    y_pred = classifier_model.predict(x_test)
    accuracy = accuracy_score(y_test, y_pred)
    logger = logging.getLogger(__name__)
    logger.info('Classifier has an overall accuracy of {}'.format(accuracy))
    return None
