from kedro.pipeline import node, Pipeline
from .nodes import model_training, evaluate_model
from typing import Dict

def create_pipeline(**kwargs) -> Dict[str, Pipeline]:
    return Pipeline(
        [
            node(
                func=model_training,
                inputs=["x_train", "y_train"],
                outputs="classifier_model",
                name="model_training_job"
            ),

            node(
                func=evaluate_model,
                inputs=['classifier_model', "x_test", "y_test"],
                outputs=None,
                name="model_evaluation"
            )
        ]
    )

